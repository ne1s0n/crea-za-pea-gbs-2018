#!/bin/bash

#this script uses GBS SNP CROP to create a mock reference genome. It is
#organized in sections enclosed in if/then statemens, so that it is possible
#to switch on and off each one.

#-------------- SETUP --------------
GBS_SNP_CROP=/home/nelson/research/GBS-SNP-CROP/GBS-SNP-CROP-scripts/v.3.0
ROOT=/home/nelson/research/GBS_novogene_pea_comparison/data/2014_12_pea_GBS_apeKi

cd $ROOT

#-------------- STEP 0 --------------
if false; then
for TARGET in CREA_PEA_R1_001 CREA_PEA_R1_002 CREA_PEA_R1_003 CREA_PEA_R1_004; do
    echo working on $TARGET
    #working with uncompressed files
    #zcat ORIGINAL_$TARGET.fastq.gz | head -n 1000 > $TARGET.fastq #only 1000 lines, for debug
    zcat ORIGINAL_$TARGET.fastq.gz > $TARGET.fastq

    #converting quality from Illumina 1.3 to Illumina 1.8
    sed -i -e '4~4y/@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghi/!"#$%&'\''()*+,-.\/0123456789:;<=>?@ABCDEFGHIJ/' $TARGET.fastq

    #converting FASTQ title line to more palatable format
    #substitute #NNNNNN/1 with 1:N:0:
    #as per https://groups.google.com/forum/#!msg/gbs-snp-crop/QcNe3HQyN-Q/Mz_1WqQMBQAJ
    #it's not enough! we need to dig into the perl code...
    sed -i 's@#NNNNNN/1@ 1:N:0:@g' $TARGET.fastq

    #compressing back to .fastq.gz
    gzip $TARGET.fastq
done
fi

#-------------- STEP 1 --------------
if false; then
#ApeKi has this residual site: CWGC
#But GBS-SNP-CROP does not support wobble bases, so we run Step 1 two times
#first time with:  CTGC
#second time with: CAGC

#first time
echo GBS_SNP_CROP Step 1 - CTGC
perl $GBS_SNP_CROP/GBS-SNP-CROP-1.pl -d SE -b barcodes_GBS_SNP_CROP_multi_parents.csv -fq CREA_PEA -s 1 -e 4 -enz1 CTGC -enz2 CTGC -t 10

#renaming folders so that the second run does not overwrite results
mv distribs distribs_CTGC
mv parsed parsed_CTGC
mv summaries summaries_CTGC

#second time
echo GBS_SNP_CROP Step 1 - CAGC
perl $GBS_SNP_CROP/GBS-SNP-CROP-1.pl -d SE -b barcodes_GBS_SNP_CROP_multi_parents.csv -fq CREA_PEA -s 1 -e 4 -enz1 CAGC -enz2 CAGC -t 10

#renaming folders to second run postfix
mv distribs distribs_CAGC
mv parsed parsed_CAGC
mv summaries summaries_CAGC

#joining the two runs
mkdir parsed
cat parsed_CAGC/CREA_PEA_001.R1parsed.fq.gz parsed_CTGC/CREA_PEA_001.R1parsed.fq.gz > parsed/CREA_PEA_001.R1parsed.fq.gz
cat parsed_CAGC/CREA_PEA_002.R1parsed.fq.gz parsed_CTGC/CREA_PEA_002.R1parsed.fq.gz > parsed/CREA_PEA_002.R1parsed.fq.gz
cat parsed_CAGC/CREA_PEA_003.R1parsed.fq.gz parsed_CTGC/CREA_PEA_003.R1parsed.fq.gz > parsed/CREA_PEA_003.R1parsed.fq.gz
cat parsed_CAGC/CREA_PEA_004.R1parsed.fq.gz parsed_CTGC/CREA_PEA_004.R1parsed.fq.gz > parsed/CREA_PEA_004.R1parsed.fq.gz

#removing the separate fastq files, to save space
rm -rf parsed_CAGC
rm -rf parsed_CTGC
fi

#-------------- STEP 2 --------------
if false; then
# Trimming Single-End (SE) reads:
perl $GBS_SNP_CROP/GBS-SNP-CROP-2.pl -tm /home/nelson/software/Trimmomatic-0.36/trimmomatic-0.36.jar -d SE -fq parsed/CREA_PEA -t 10 -ph 33 -ad /home/nelson/software/Trimmomatic-0.36/adapters/TruSeq3-SE.fa:2:30:10 -l 30 -sl 4:30 -tr 30 -m 32
fi

#-------------- STEP 3 --------------
if false; then
  # Demultiplexing Single-End (SE) reads:
  cd $ROOT/parsed
  perl $GBS_SNP_CROP/GBS-SNP-CROP-3_performance.pl -d SE -b ../barcodes_GBS_SNP_CROP_multi_parents.csv -fq CREA_PEA
  
  #since I have several samples with different barcodes I had to rename them accordingly
  #to avoid conflict. Time to put the data together
  cd $ROOT/parsed/demultiplexed
  cat ATTIKA?.R1.fq.gz > ATTIKA.R1.fq.gz
  rm ATTIKA?.R1.fq.gz
  cat ISARD?.R1.fq.gz > ISARD.R1.fq.gz
  rm ISARD?.R1.fq.gz
  cat KASPA?.R1.fq.gz > KASPA.R1.fq.gz
  rm KASPA?.R1.fq.gz
fi

#-------------- STEP 4 --------------
if false; then
cd $ROOT/parsed/demultiplexed
perl $GBS_SNP_CROP/GBS-SNP-CROP-4.pl -d SE -b ../../barcodes_GBS_SNP_CROP_single_parents.csv -rl 100 -pl 64 -p 0.01 -id 0.93 -t 8 -MR MRef -db 5 -rs TRUE
rm -rf fastaForRef/
fi
